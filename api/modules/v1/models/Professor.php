<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "professor".
 *
 * @property integer $id
 * @property string $matriculation
 *
 * @property ProfessorHasDiscipline[] $professorHasDisciplines
 * @property Discipline[] $disciplines
 * @property User[] $users
 */
class Professor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'professor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['matriculation'], 'required'],
            [['matriculation'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matriculation' => 'Matriculation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessorHasDisciplines()
    {
        return $this->hasMany(ProfessorHasDiscipline::className(), ['professor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->hasMany(Discipline::className(), ['id' => 'discipline_id'])->viaTable('professor_has_discipline', ['professor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['professor_id' => 'id']);
    }
}
