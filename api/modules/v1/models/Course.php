<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "course".
 *
 * @property integer $id
 * @property string $name
 * @property integer $period
 * @property string $code
 *
 * @property CourseHasDiscipline[] $courseHasDisciplines
 * @property Discipline[] $disciplines
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'period', 'code'], 'required'],
            [['period'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }


    public function extraFields()
    {
        $fields = parent::extraFields();
        $fields['disciplines'] = 'disciplines';
        return $fields;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'period' => 'Period',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseHasDisciplines()
    {
        return $this->hasMany(CourseHasDiscipline::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->hasMany(Discipline::className(), ['id' => 'discipline_id'])->viaTable('course_has_discipline', ['course_id' => 'id']);
    }

    public static function addDisciplines($id)
    {
        $disciplines = Yii::$app->request->post('disciplines');
        foreach ($disciplines as $discipline) {
            $courseHasDiscipline = new CourseHasDiscipline();
            $courseHasDiscipline->course_id = $id;
            $courseHasDiscipline->discipline_id = $discipline['id'];
            $courseHasDiscipline->save();
        }

        return Yii::$app->response->setStatusCode(200);
    }

    public static function removeDiscipline($id, $discipline_id)
    {
        CourseHasDiscipline::deleteAll(['course_id' => $id, 'discipline_id' => $discipline_id]);

        return Yii::$app->response->setStatusCode(200);
    }
}
