<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "workload".
 *
 * @property integer $id
 * @property string $name
 * @property integer $total
 * @property integer $discipline_id
 *
 * @property Discipline $discipline
 */
class Workload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'workload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'total', 'discipline_id'], 'required'],
            [['total', 'discipline_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['discipline_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'total' => 'Total',
            'discipline_id' => 'Discipline ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['id' => 'discipline_id']);
    }
}
