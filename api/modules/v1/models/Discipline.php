<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "discipline".
 *
 * @property integer $id
 * @property string $name
 *
 * @property CourseHasDiscipline[] $courseHasDisciplines
 * @property Course[] $courses
 * @property Practice[] $practices
 * @property ProfessorHasDiscipline[] $professorHasDisciplines
 * @property Professor[] $professors
 * @property Workload[] $workloads
 */
class Discipline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ade_workload' => 'Ade Workload',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseHasDisciplines()
    {
        return $this->hasMany(CourseHasDiscipline::className(), ['discipline_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['id' => 'course_id'])->viaTable('course_has_discipline', ['discipline_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractices()
    {
        return $this->hasMany(Practice::className(), ['discipline_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessorHasDisciplines()
    {
        return $this->hasMany(ProfessorHasDiscipline::className(), ['discipline_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessors()
    {
        return $this->hasMany(Professor::className(), ['id' => 'professor_id'])->viaTable('professor_has_discipline', ['discipline_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkloads()
    {
        return $this->hasMany(Workload::className(), ['discipline_id' => 'id']);
    }
}
