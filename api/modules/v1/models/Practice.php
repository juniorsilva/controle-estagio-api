<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "practice".
 *
 * @property integer $id
 * @property string $local
 * @property string $activity
 * @property string $observation
 * @property string $started_at
 * @property string $ended_at
 * @property string $schedule
 * @property integer $student_id
 * @property integer $discipline_id
 *
 * @property Discipline $discipline
 * @property Student $student
 */
class Practice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'practice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['local', 'activity', 'student_id', 'discipline_id'], 'required'],
            [['activity', 'observation', 'schedule'], 'string'],
            [['started_at', 'ended_at'], 'safe'],
            [['student_id', 'discipline_id'], 'integer'],
            [['local'], 'string', 'max' => 255],
            [['discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['discipline_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();

        $fields['started_at'] = function ($model) {
            /** @var static $model */
            return (!empty($this->started_at)) ? date(DATE_W3C, strtotime($model->started_at)) : NULL;
        };

        $fields['ended_at'] = function ($model) {
            /** @var static $model */
            return (!empty($this->ended_at)) ? date(DATE_W3C, strtotime($model->ended_at)) : NULL;
        };

        return $fields;
    }

    public function extraFields()
    {
        $fields = parent::extraFields();
        $fields['student'] = 'student';
        $fields['discipline'] = 'discipline';
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'local' => 'Local',
            'activity' => 'Activity',
            'observation' => 'Observation',
            'started_at' => 'Started At',
            'ended_at' => 'Ended At',
            'schedule' => 'Schedule',
            'student_id' => 'Student ID',
            'discipline_id' => 'Discipline ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['id' => 'discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    public function beforeSave($insert)
    {
        if (!empty($this->started_at))
            $this->started_at = date('Y-m-d', strtotime($this->started_at));

        if (!empty($this->ended_at))
            $this->ended_at = date('Y-m-d', strtotime($this->ended_at));

        return parent::beforeSave($insert);
    }
}
