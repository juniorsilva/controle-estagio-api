<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Course;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;

/**
 * Course Controller API
 */
class CourseController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Course';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['options']
        ];

        return $behaviors;
    }


    public function actionAddDisciplines($id)
    {
        Course::addDisciplines($id);
    }

    public function actionRemoveDiscipline($id, $discipline_id)
    {
        Course::removeDiscipline($id, $discipline_id);
    }
}