<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142620_professor_has_discipline extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%professor_has_discipline}}',
            [
                'professor_id'=> $this->integer(11)->notNull(),
                'discipline_id'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('fk_professor_has_discipline_discipline1_idx','{{%professor_has_discipline}}',['discipline_id'],false);
        $this->createIndex('fk_professor_has_discipline_professor1_idx','{{%professor_has_discipline}}',['professor_id'],false);
        $this->addPrimaryKey('pk_on_professor_has_discipline','{{%professor_has_discipline}}',['professor_id','discipline_id']);

    }

    public function safeDown()
    {
    $this->dropPrimaryKey('pk_on_professor_has_discipline','{{%professor_has_discipline}}');
        $this->dropIndex('fk_professor_has_discipline_discipline1_idx', '{{%professor_has_discipline}}');
        $this->dropIndex('fk_professor_has_discipline_professor1_idx', '{{%professor_has_discipline}}');
        $this->dropTable('{{%professor_has_discipline}}');
    }
}
