<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142616_course_has_discipline extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%course_has_discipline}}',
            [
                'course_id'=> $this->integer(11)->notNull(),
                'discipline_id'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('fk_course_has_discipline_discipline1_idx','{{%course_has_discipline}}',['discipline_id'],false);
        $this->createIndex('fk_course_has_discipline_course1_idx','{{%course_has_discipline}}',['course_id'],false);
        $this->addPrimaryKey('pk_on_course_has_discipline','{{%course_has_discipline}}',['course_id','discipline_id']);

    }

    public function safeDown()
    {
    $this->dropPrimaryKey('pk_on_course_has_discipline','{{%course_has_discipline}}');
        $this->dropIndex('fk_course_has_discipline_discipline1_idx', '{{%course_has_discipline}}');
        $this->dropIndex('fk_course_has_discipline_course1_idx', '{{%course_has_discipline}}');
        $this->dropTable('{{%course_has_discipline}}');
    }
}
