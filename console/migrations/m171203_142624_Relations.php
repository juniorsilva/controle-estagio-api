<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142624_Relations extends Migration
{

    public function init()
    {
       $this->db = 'db';
       parent::init();
    }

    public function safeUp()
    {
        $this->addForeignKey('fk_course_has_discipline_course_id',
            '{{%course_has_discipline}}','course_id',
            '{{%course}}','id',
            'CASCADE','NO ACTION'
         );
        $this->addForeignKey('fk_course_has_discipline_discipline_id',
            '{{%course_has_discipline}}','discipline_id',
            '{{%discipline}}','id',
            'CASCADE','NO ACTION'
         );
        $this->addForeignKey('fk_practice_discipline_id',
            '{{%practice}}','discipline_id',
            '{{%discipline}}','id',
            'CASCADE','NO ACTION'
         );
        $this->addForeignKey('fk_practice_student_id',
            '{{%practice}}','student_id',
            '{{%student}}','id',
            'CASCADE','NO ACTION'
         );
        $this->addForeignKey('fk_professor_has_discipline_discipline_id',
            '{{%professor_has_discipline}}','discipline_id',
            '{{%discipline}}','id',
            'CASCADE','NO ACTION'
         );
        $this->addForeignKey('fk_professor_has_discipline_professor_id',
            '{{%professor_has_discipline}}','professor_id',
            '{{%professor}}','id',
            'CASCADE','NO ACTION'
         );
        $this->addForeignKey('fk_user_professor_id',
            '{{%user}}','professor_id',
            '{{%professor}}','id',
            'CASCADE','NO ACTION'
         );
        $this->addForeignKey('fk_workload_discipline_id',
            '{{%workload}}','discipline_id',
            '{{%discipline}}','id',
            'CASCADE','NO ACTION'
         );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_course_has_discipline_course_id', '{{%course_has_discipline}}');
        $this->dropForeignKey('fk_course_has_discipline_discipline_id', '{{%course_has_discipline}}');
        $this->dropForeignKey('fk_practice_discipline_id', '{{%practice}}');
        $this->dropForeignKey('fk_practice_student_id', '{{%practice}}');
        $this->dropForeignKey('fk_professor_has_discipline_discipline_id', '{{%professor_has_discipline}}');
        $this->dropForeignKey('fk_professor_has_discipline_professor_id', '{{%professor_has_discipline}}');
        $this->dropForeignKey('fk_user_professor_id', '{{%user}}');
        $this->dropForeignKey('fk_workload_discipline_id', '{{%workload}}');
    }
}
