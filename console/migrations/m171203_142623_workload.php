<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142623_workload extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%workload}}',
            [
                'id'=> $this->primaryKey(11),
                'name'=> $this->string(255)->notNull(),
                'total'=> $this->integer(11)->notNull(),
                'discipline_id'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('fk_workload_discipline_idx','{{%workload}}',['discipline_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('fk_workload_discipline_idx', '{{%workload}}');
        $this->dropTable('{{%workload}}');
    }
}
