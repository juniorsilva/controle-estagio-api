<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142622_user extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user}}',
            [
                'id'=> $this->primaryKey(11),
                'first_name'=> $this->string(255)->notNull(),
                'last_name'=> $this->string(255)->notNull(),
                'email'=> $this->string(255)->notNull(),
                'encrypted_password'=> $this->string(255)->notNull(),
                'user_type'=> $this->string(255)->notNull(),
                'professor_id'=> $this->integer(11)->null()->defaultValue(null),
                'access_token'=> $this->string(255)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('fk_user_professor1_idx','{{%user}}',['professor_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('fk_user_professor1_idx', '{{%user}}');
        $this->dropTable('{{%user}}');
    }
}
