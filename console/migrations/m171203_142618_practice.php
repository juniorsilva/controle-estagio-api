<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142618_practice extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%practice}}',
            [
                'id'=> $this->primaryKey(11),
                'local'=> $this->string(255)->notNull(),
                'activity'=> $this->text()->notNull(),
                'observation'=> $this->text()->null()->defaultValue(null),
                'started_at'=> $this->date()->null()->defaultValue(null),
                'ended_at'=> $this->date()->null()->defaultValue(null),
                'schedule'=> $this->text()->null()->defaultValue(null),
                'student_id'=> $this->integer(11)->notNull(),
                'discipline_id'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('fk_practice_student1_idx','{{%practice}}',['student_id'],false);
        $this->createIndex('fk_practice_discipline1_idx','{{%practice}}',['discipline_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('fk_practice_student1_idx', '{{%practice}}');
        $this->dropIndex('fk_practice_discipline1_idx', '{{%practice}}');
        $this->dropTable('{{%practice}}');
    }
}
