<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142655_userDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('{{%user}}',
                           ["id", "first_name", "last_name", "email", "encryped_password", "user_type", "professor_id", "access_token"],
                            [
    [
        'id' => '1',
        'first_name' => 'Admin',
        'last_name' => 'do Sistema',
        'email' => 'admin@adm.in',
        'encryped_password' => '$2y$13$p0XAhZtUfOHUi7V8gpSV7ekmQG9rd3vPtrwulX7DwL/cMv3RQ0jVS',
        'user_type' => '0',
        'professor_id' => null,
        'access_token' => '6MFNbyIdmrnayXeSd4z-ASxzysPWSldV',
    ],
]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%user}} CASCADE');
    }
}
