<?php

use yii\db\Schema;
use yii\db\Migration;

class m171203_142619_professor extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%professor}}',
            [
                'id' => $this->primaryKey(11),
                'matriculation' => $this->string(255)->notNull(),
            ], $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%professor}}');
    }
}
